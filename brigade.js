const { events, Job } = require("brigadier");

function runSuite(e, p) {
  console.log("received push for commit " + e.revision.commit);

  // Create a new job
  var testRunner = new Job("test-runner");

  // We want our job to run the stock Docker Python 3 image
  testRunner.image = "debian:latest";

  // Now we want it to run these commands in order:
  testRunner.tasks = [
    "ls /src/"
  ];

  // Display logs from the job Pod
  testRunner.streamLogs = true;

  // We're done configuring, so we run the job
  testRunner.run();
}

events.on("pullrequest:created", runSuite);
events.on("pullrequest:updated", runSuite);
